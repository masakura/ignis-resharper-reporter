﻿using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using Ignis.ReSharper.Reporter.InspectCode;
using Ignis.ReSharper.Reporter.Tool.Options;

namespace Ignis.ReSharper.Reporter.Tool;

internal sealed class ReSharperReporterCommand
{
    private readonly IConsole? _console;
    private readonly RootCommand _root = Create();

    public ReSharperReporterCommand(IConsole? console = null)
    {
        _console = console;
    }

    private static ToolOptions Options => ToolOptions.Instance;

    public async Task<int> InvokeAsync(string[] args)
    {
        return await _root.InvokeAsync(args, _console);
    }

    private static RootCommand Create()
    {
        var command = new RootCommand
        {
            Handler = new ReportCommandHandler(),
            Description = "Convert from ReSharper InspectCode report to GitLab CodeQuality report."
        };
        Options.AddTo(command);
        return command;
    }

    private sealed class ReportCommandHandler : ICommandHandler
    {
        public Task<int> InvokeAsync(InvocationContext context)
        {
            try
            {
                PerformInvokeAsync(context);
            }
            catch (InvalidOperationException ex)
            {
                context.Console.Error.WriteLine(ex.ToString());
                return Task.FromResult(2);
            }

            return Task.FromResult(0);
        }

        private static void PerformInvokeAsync(InvocationContext context)
        {
            var console = new ConsoleLogger(context.Console);

            var report = InspectCodeReport.Load(Options.Input.GetFrom(context).FullName, console);
            report.Export(Options.Export.GetFrom(context));

            var severity = Options.Severity.TryGetFrom(context);
            if (severity != null) report.EnsureNoIssues(severity);
        }
    }
}
