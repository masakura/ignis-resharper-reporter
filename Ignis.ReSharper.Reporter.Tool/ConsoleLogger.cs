﻿using System.CommandLine;
using Ignis.ReSharper.Reporter.Logging;

namespace Ignis.ReSharper.Reporter.Tool;

internal sealed class ConsoleLogger : Logger
{
    private readonly IConsole _console;

    public ConsoleLogger(IConsole console)
    {
        _console = console;
    }

    public override void WriteLine(string message)
    {
        _console.WriteLine(message);
    }
}
