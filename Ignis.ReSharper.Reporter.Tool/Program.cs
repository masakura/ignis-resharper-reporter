﻿using Ignis.ReSharper.Reporter.Tool;

var command = new ReSharperReporterCommand();

return await command.InvokeAsync(Environment.GetCommandLineArgs());
