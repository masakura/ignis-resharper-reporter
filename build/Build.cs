using Builds;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.Execution;

[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild, IClean, ILint, ITest, IPush, ICleanupCode
{
    public static int Main() => Execute<Build>(x => ((ICompile) x).Compile);
}
