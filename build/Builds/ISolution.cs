﻿using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;

namespace Builds;

public interface ISolution : INukeBuild
{
    [Solution] Solution Solution => TryGetValue(() => Solution);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    Configuration Configuration => TryGetValue(() => Configuration) ?? DefaultConfiguration();

    AbsolutePath OutputDirectory => RootDirectory / "output";
    AbsolutePath CacheDirectory => RootDirectory / ".cache";

    private Configuration DefaultConfiguration() => IsLocalBuild ? Configuration.Debug : Configuration.Release;
}
