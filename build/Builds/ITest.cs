﻿using Nuke.Common;
using Nuke.Common.Tools.DotNet;

namespace Builds;

public interface ITest : ICompile
{
    private string ReportFile => OutputDirectory / "reports" / "junit" / "{assembly}-test-result.xml";

    // ReSharper disable once UnusedMember.Global
    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetTasks.DotNetTest(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .AddLoggers($"junit;LogFilePath={ReportFile};MethodFormat=Class;FailureBodyFormat=Verbose")
                .EnableNoBuild());
        });
}
