﻿using Nuke.Common;
using Nuke.Common.IO;

namespace Builds;

public interface IClean : ISolution
{
    // ReSharper disable once UnusedMember.Global
    Target Clean => _ => _
        .Executes(() =>
        {
            FileSystemTasks.EnsureCleanDirectory(OutputDirectory);
        });
}
