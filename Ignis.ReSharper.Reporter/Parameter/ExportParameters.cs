﻿using System.Collections.ObjectModel;

namespace Ignis.ReSharper.Reporter.Parameter;

public sealed class ExportParameters
{
    private readonly KeyedCollection _items;

    public ExportParameters() : this(Enumerable.Empty<ExportParameter>())
    {
    }

    private ExportParameters(IEnumerable<ExportParameter> parameters)
    {
        _items = new KeyedCollection(parameters);
    }

    public static ExportParameters Empty { get; } = new();

    public static ExportParameters Parse(string parametersString)
    {
        var parameters =
            from parameter in parametersString.Split(';')
            select ExportParameter.Parse(parameter);

        return new ExportParameters(parameters);
    }

    public string Value(string key)
    {
        return _items[key].Value;
    }

    public string? TryValue(string key)
    {
        if (_items.Contains(key)) return _items[key].Value;
        return null;
    }

    public ExportParameters Add(string key, string value)
    {
        var @new = new KeyedCollection(_items) {new(key, value)};
        return new ExportParameters(@new);
    }

    public ExportParameters Union(ExportParameters other)
    {
        return new ExportParameters(_items.Union(other._items));
    }

    public override string ToString()
    {
        return string.Join(";", _items);
    }

    private sealed class KeyedCollection : KeyedCollection<string, ExportParameter>
    {
        public KeyedCollection(IEnumerable<ExportParameter> parameters)
        {
            foreach (var parameter in parameters) Add(parameter);
        }

        protected override string GetKeyForItem(ExportParameter item)
        {
            return item.Key;
        }
    }
}
