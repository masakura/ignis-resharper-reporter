﻿namespace Ignis.ReSharper.Reporter.Parameter;

public sealed class ParameterParseException : Exception
{
    public ParameterParseException(string message) : base(message)
    {
    }
}
