﻿using System.Text.RegularExpressions;

namespace Ignis.ReSharper.Reporter.Parameter;

internal sealed record ExportParameter(string Key, string Value)
{
    public string Key { get; } = Key;
    public string Value { get; } = Value;

    public static ExportParameter Parse(string parameter)
    {
        var match = Regex.Match(parameter, "^([^=]*)=(.*)$");
        if (!match.Success)
            throw new ParameterParseException($"Parameter require `key1=value1;key2=value2`. ({parameter})");
        return new ExportParameter(match.Groups[1].Value, match.Groups[2].Value);
    }

    public override string ToString()
    {
        return $"{Key}={Value}";
    }
}
