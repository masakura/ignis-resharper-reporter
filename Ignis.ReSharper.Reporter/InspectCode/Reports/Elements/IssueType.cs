﻿using System.Xml.Linq;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record IssueType(string Id, Severity Severity, string? Description, Category Category)
{
    public string Id { get; } = Id;
    public Severity Severity { get; } = Severity;
    public string? Description { get; } = Description;
    public Category Category { get; } = Category;

    private static IssueType Get(XElement element)
    {
        var category = new Category(
            element.Attribute("CategoryId")?.Value ?? throw new InvalidOperationException(),
            element.Attribute("Category")?.Value ?? throw new InvalidOperationException());

        var id = element.Attribute(nameof(Id))?.Value ?? throw new InvalidOperationException();

        return new IssueType(id,
            Severity.Get(element.Attribute(nameof(Elements.Severity))?.Value),
            element.Attribute(nameof(Description))?.Value,
            category);
    }

    public static IEnumerable<IssueType> All(XElement element)
    {
        return element.Elements(nameof(IssueType)).Select(Get);
    }
}
