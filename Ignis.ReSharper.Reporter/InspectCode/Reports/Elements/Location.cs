﻿using System.Xml.Linq;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record Location(string File, Offset Offset, int Line)
{
    public Location(string file, string offset, int line) : this(file, Offset.Parse(offset), line)
    {
    }

    public string File { get; } = File;
    public Offset Offset { get; } = Offset;
    public int Line { get; } = Line;

    public static Location Get(XElement element)
    {
        var file = element.Attribute(nameof(File))?.Value ?? throw new InvalidOperationException();
        var offset = element.Attribute(nameof(Offset))?.Value ?? throw new InvalidOperationException();
        // https://gitlab.com/ignis-build/ignis-resharper-reporter/-/issues/21
        var line = element.Attribute(nameof(Line))?.Value ?? "1";

        return new Location(file,
            Offset.Parse(offset),
            int.Parse(line));
    }
}
