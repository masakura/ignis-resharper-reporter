﻿using System.Xml.Linq;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record InspectionScope(string? Element)
{
    public string? Element { get; } = Element;

    public static InspectionScope Get(XElement? element)
    {
        var inspectionScope = element?.Element(nameof(InspectionScope));

        if (inspectionScope == null) return new InspectionScope((string?) null);

        return new InspectionScope(inspectionScope.Element(nameof(Element))?.Value);
    }
}
