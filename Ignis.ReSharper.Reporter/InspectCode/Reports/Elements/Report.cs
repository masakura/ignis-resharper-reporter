﻿using System.Xml.Linq;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record Report(string? ToolsVersion,
    Information Information,
    IssueTypeCollection? IssueTypes = null,
    ProjectCollection? Issues = null)
{
    public string? ToolsVersion { get; } = ToolsVersion;
    public Information Information { get; } = Information;
    public IssueTypeCollection IssueTypes { get; } = IssueTypes ?? IssueTypeCollection.Empty;
    public ProjectCollection Issues { get; } = Issues ?? ProjectCollection.Empty;

    public static Report Load(string path)
    {
        var document = XDocument.Load(path);
        return Get(document);
    }

    private static Report Get(XDocument document)
    {
        return Get(document.Root);
    }

    private static Report Get(XElement? element)
    {
        var issueTypes = IssueTypeCollection.Get(element);

        return new Report(element?.Attribute(nameof(ToolsVersion))?.Value,
            Information.Get(element),
            issueTypes,
            ProjectCollection.Get(element, issueTypes));
    }
}
