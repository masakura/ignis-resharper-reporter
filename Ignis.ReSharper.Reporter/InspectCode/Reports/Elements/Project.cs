﻿using System.Xml.Linq;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record Project(string Name, IssueCollection Issues)
{
    public string Name { get; } = Name;
    public IssueCollection Issues { get; } = Issues;

    private static Project Get(XElement element, IssueTypeCollection issueTypes)
    {
        var name = element.Attribute(nameof(Name))?.Value ?? throw new InvalidOperationException();

        return new Project(name, IssueCollection.Get(element, issueTypes));
    }

    public static IEnumerable<Project> All(XElement element, IssueTypeCollection issueTypes)
    {
        return element.Elements(nameof(Project)).Select(e => Get(e, issueTypes));
    }
}
