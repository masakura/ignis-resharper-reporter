﻿// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed record Offset(int Begin, int End)
{
    public int Begin { get; } = Begin;
    public int End { get; } = End;

    public override string ToString()
    {
        return $"{Begin}-{End}";
    }

    public static Offset Parse(string? value)
    {
        if (value == null) throw new ArgumentNullException(nameof(value));

        var parts = value.Split('-');
        if (parts.Length != 2) throw new InvalidOperationException(@"Require ""10-20"".");

        return new Offset(int.Parse(parts[0]), int.Parse(parts[1]));
    }
}
