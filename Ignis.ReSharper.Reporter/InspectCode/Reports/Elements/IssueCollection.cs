﻿using System.Xml.Linq;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class IssueCollection : ElementCollection<Issue>
{
    // ReSharper disable once MemberCanBePrivate.Global
    public IssueCollection(IEnumerable<Issue> items) : base(items)
    {
    }

    public static implicit operator IssueCollection(Issue[] items)
    {
        return new IssueCollection(items);
    }

    public static IssueCollection Get(XElement element, IssueTypeCollection issueTypes)
    {
        return new IssueCollection(Issue.All(element, issueTypes));
    }

    public IssueCollection Where(Severity severity)
    {
        return new IssueCollection(Items.Where(i => i.Is(severity)));
    }

    /// <summary>
    ///     Ensure no issues.
    /// </summary>
    public void EnsureNoIssues()
    {
        if (!IsEmpty()) throw new InvalidOperationException("Found problems in code.");
    }
}
