﻿namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class IssueFactory
{
    private readonly IssueTypeCollection _issueTypes;

    public IssueFactory(IssueTypeCollection issueTypes)
    {
        _issueTypes = issueTypes;
    }

    public Issue Create(string typeId, string? message, Location location)
    {
        return new Issue(message, location, _issueTypes.Find(typeId));
    }
}
