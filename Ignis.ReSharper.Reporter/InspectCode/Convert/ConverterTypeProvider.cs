﻿using System.Collections.ObjectModel;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert;

internal sealed class ConverterTypeProvider
{
    private static readonly Lazy<ConverterTypeProvider> Lazy = new(() => new ConverterTypeProvider(Create()));
    private readonly KeyedCollection _items;

    private ConverterTypeProvider(IEnumerable<ReportConverterType> items)
    {
        _items = new KeyedCollection(items);
    }

    public static ConverterTypeProvider Instance => Lazy.Value;

    private static IEnumerable<ReportConverterType> Create()
    {
        return AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Select(ReportConverterType.TryCreate)
            .OfType<ReportConverterType>();
    }

    public ReportConverterType Find(string type)
    {
        return _items[type];
    }

    private sealed class KeyedCollection : KeyedCollection<string, ReportConverterType>
    {
        public KeyedCollection(IEnumerable<ReportConverterType> items)
        {
            foreach (var type in items) Add(type);
        }

        protected override string GetKeyForItem(ReportConverterType item)
        {
            return item.Name;
        }
    }
}
