﻿using System.Reflection;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert;

[AttributeUsage(AttributeTargets.Class)]
public sealed class ReportConverterAttribute : Attribute
{
    private readonly string _name;

    public ReportConverterAttribute(string name)
    {
        _name = name;
    }

    internal static string? TryGetExporterTypeName(Type type)
    {
        if (!typeof(IReportConverter).IsAssignableFrom(type)) return null;
        var attribute = type.GetCustomAttribute<ReportConverterAttribute>();
        return attribute?._name ?? type.Name;
    }
}
