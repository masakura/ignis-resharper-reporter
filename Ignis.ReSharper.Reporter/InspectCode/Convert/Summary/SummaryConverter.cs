﻿using ConsoleTables;
using Ignis.ReSharper.Reporter.InspectCode.Reports;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;

[ReportConverter("summary")]
// ReSharper disable once UnusedType.Global
// ReSharper disable once UnusedMember.Global
public sealed class SummaryConverter : IReportConverter
{
    public void Export(Report report, Stream stream)
    {
        using var writer = new StreamWriter(stream, null!, -1, true);

        var statistics = report.Statistics();

        ConsoleTable.From(SeverityCount.Rows(statistics))
            .Configure(o =>
            {
                // ReSharper disable once AccessToDisposedClosure
                o.OutputTo = writer;
                o.NumberAlignment = Alignment.Right;
            })
            .Write(Format.Minimal);
    }
}
