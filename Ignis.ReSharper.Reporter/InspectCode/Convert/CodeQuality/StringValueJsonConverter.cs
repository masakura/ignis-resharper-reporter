﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal sealed class StringValueJsonConverter<T> : JsonConverter<T>
{
    public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        throw new InvalidOperationException();
    }

    public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value?.ToString());
    }
}
