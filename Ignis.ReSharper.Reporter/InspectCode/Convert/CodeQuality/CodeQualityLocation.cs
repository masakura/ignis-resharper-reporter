﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal sealed record CodeQualityLocation(FilePath Path, CodeQualityLocationLines Lines)
{
    public CodeQualityLocation(Location location) :
        this(location.File, location.Line)
    {
    }

    public CodeQualityLocation(string path, int linesBegin) :
        this(new FilePath(path), new CodeQualityLocationLines(linesBegin))
    {
    }

    public FilePath Path { get; } = Path;
    public CodeQualityLocationLines Lines { get; } = Lines;
}
