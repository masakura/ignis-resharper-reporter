﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal static class CodeQualityIssueExtensions
{
    public static IEnumerable<CodeQualityIssue> ToCodeQualityIssues(this Report report)
    {
        return report.Issues.All().ToCodeQualityIssues();
    }

    private static IEnumerable<CodeQualityIssue> ToCodeQualityIssues(this IssueCollection issues)
    {
        return from issue in issues.AsEnumerable()
            select new CodeQualityIssue(issue);
    }
}
