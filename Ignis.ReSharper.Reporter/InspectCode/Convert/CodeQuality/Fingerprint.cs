﻿using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

[JsonConverter(typeof(StringValueJsonConverter<Fingerprint>))]
internal sealed class Fingerprint : IEquatable<Fingerprint>
{
    private readonly string _value;

    public Fingerprint(CodeQualityIssueWithoutFingerprint issue) : this(Compute(issue))
    {
    }

    private Fingerprint(string value)
    {
        _value = value;
    }

    public bool Equals(Fingerprint? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _value == other._value;
    }

    public override string ToString()
    {
        return _value;
    }

    private static string Compute(CodeQualityIssueWithoutFingerprint issue)
    {
        var json = JsonSerializer.Serialize(issue);
        var sha1 = SHA1.Create();
        var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(json));
        return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is Fingerprint other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public static bool operator ==(Fingerprint? left, Fingerprint? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(Fingerprint? left, Fingerprint? right)
    {
        return !Equals(left, right);
    }
}
