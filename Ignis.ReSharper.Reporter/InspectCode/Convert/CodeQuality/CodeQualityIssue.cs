﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal sealed record CodeQualityIssue(string? Description,
    CodeQualitySeverity Severity,
    CodeQualityLocation Location) :
    CodeQualityIssueWithoutFingerprint(Description, Severity, Location)
{
    public CodeQualityIssue(Issue issue) :
        this(issue.Message,
            issue.Severity.ToCodeQualitySeverity(),
            issue.Location.ToCodeQualityLocation())
    {
    }

    public CodeQualityIssue(string description, CodeQualitySeverity severity, string path, int linesBegin) :
        this(description, severity, new CodeQualityLocation(path, linesBegin))
    {
    }

    public Fingerprint Fingerprint => new(this);
}
