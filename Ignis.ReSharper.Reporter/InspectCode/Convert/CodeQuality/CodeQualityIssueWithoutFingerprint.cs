﻿// ReSharper disable UnusedMember.Global

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

internal abstract record CodeQualityIssueWithoutFingerprint(string? Description,
    CodeQualitySeverity Severity,
    CodeQualityLocation Location)
{
    public string? Description { get; } = Description;
    public CodeQualitySeverity Severity { get; } = Severity;
    public CodeQualityLocation Location { get; } = Location;
}
