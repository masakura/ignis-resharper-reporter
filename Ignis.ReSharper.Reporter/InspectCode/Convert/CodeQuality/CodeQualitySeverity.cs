﻿using System.Text.Json.Serialization;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

[JsonConverter(typeof(StringValueJsonConverter<CodeQualitySeverity>))]
public sealed class CodeQualitySeverity
{
    private readonly string _name;

    private CodeQualitySeverity(string name)
    {
        _name = name.ToLowerInvariant();
    }

    public static CodeQualitySeverity Info { get; } = new(nameof(Info));
    public static CodeQualitySeverity Minor { get; } = new(nameof(Minor));
    public static CodeQualitySeverity Major { get; } = new(nameof(Major));
    public static CodeQualitySeverity Critical { get; } = new(nameof(Critical));

    public override string ToString()
    {
        return _name;
    }

    public static CodeQualitySeverity Get(Severity severity)
    {
        if (severity == Severity.Info) return Info;
        if (severity == Severity.Hint) return Info;
        if (severity == Severity.Suggestion) return Minor;
        if (severity == Severity.Warning) return Major;
        if (severity == Severity.Error) return Critical;

        throw new ArgumentException(nameof(severity));
    }
}
