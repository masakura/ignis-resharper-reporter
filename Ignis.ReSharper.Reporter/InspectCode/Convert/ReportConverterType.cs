﻿using Ignis.ReSharper.Reporter.Parameter;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert;

internal sealed class ReportConverterType : IEquatable<ReportConverterType>
{
    private readonly Type _type;

    public ReportConverterType(string name, Type type)
    {
        Name = name;
        _type = type;
    }

    public string Name { get; }

    public bool Equals(ReportConverterType? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _type == other._type && Name == other.Name;
    }

    public IReportConverter Instance()
    {
        return (IReportConverter) Activator.CreateInstance(_type);
    }

    public static ReportConverterType Create<T>()
        where T : IReportConverter, new()
    {
        return TryCreate(typeof(T))!;
    }

    public static ReportConverterType? TryCreate(Type type)
    {
        var name = ReportConverterAttribute.TryGetExporterTypeName(type);
        if (name == null) return null;
        return new ReportConverterType(name, type);
    }

    public static ReportConverterType Parse(string parameters)
    {
        return Parse(ExportParameters.Parse(parameters));
    }

    public static ReportConverterType Parse(ExportParameters parameters)
    {
        return ConverterTypeProvider.Instance.Find(parameters.Value("type"));
    }

    public override string ToString()
    {
        return $"{Name}:{_type}";
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is ReportConverterType other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (_type.GetHashCode() * 397) ^ Name.GetHashCode();
        }
    }

    public static bool operator ==(ReportConverterType? left, ReportConverterType? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(ReportConverterType? left, ReportConverterType? right)
    {
        return !Equals(left, right);
    }
}
