﻿using Ignis.ReSharper.Reporter.InspectCode.Convert;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.Logging;
using Ignis.ReSharper.Reporter.Out;
using Ignis.ReSharper.Reporter.Parameter;

namespace Ignis.ReSharper.Reporter.InspectCode;

/// <summary>
///     Report exporter.
/// </summary>
public sealed class ExportReport : IEquatable<ExportReport>
{
    private readonly IReportConverter _converter;
    private readonly Output _output;

    /// <summary>
    /// </summary>
    /// <param name="converterType">Converter type.</param>
    /// <param name="output">Output file.</param>
    internal ExportReport(ReportConverterType converterType, Output output)
    {
        _converter = converterType.Instance();
        _output = output;
    }

    public bool Equals(ExportReport? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _converter.Equals(other._converter) && _output.Equals(other._output);
    }

    internal void Export(Report report, Logger logger)
    {
        logger.WriteLine(string.Empty);

        using var stream = _output.Stream();
        _converter.Export(report, stream);
        _output.Complete(logger);
    }

    public override string ToString()
    {
        var type = ReportConverterAttribute.TryGetExporterTypeName(_converter.GetType());

        var parameters = new ExportParameters().Add("type", type!).Union(_output.ToParameters());

        return parameters.ToString();
    }

    /// <summary>
    ///     Parse export report definition.
    /// </summary>
    /// <param name="parameters">export report parameters. (Ex: `type=codequality;file=codequality.json)`)</param>
    /// <returns>export report definition.</returns>
    public static ExportReport Parse(string parameters)
    {
        var p = ExportParameters.Parse(parameters);
        var exporterType = ReportConverterType.Parse(p);

        return new ExportReport(exporterType, Output.Parse(p));
    }

    /// <summary>
    ///     Parse export report definition.
    /// </summary>
    /// <param name="parameters">export report parameters. (Ex: `type=codequality;file=codequality.json)`)</param>
    /// <returns>export report definition.</returns>
    public static IEnumerable<ExportReport> Parse(IEnumerable<string> parameters)
    {
        return parameters.Select(Parse);
    }

    /// <summary>
    ///     Create export report definition.
    /// </summary>
    /// <param name="output">Output file.</param>
    /// <typeparam name="T">Converter type.</typeparam>
    /// <returns>export report definition.</returns>
    public static ExportReport Create<T>(string output)
        where T : IReportConverter, new()
    {
        return Create<T>(Output.File(output));
    }

    /// <summary>
    ///     Create export report definition.
    /// </summary>
    /// <param name="output">Output.</param>
    /// <typeparam name="T">Converter type.</typeparam>
    /// <returns>export report definition.</returns>
    public static ExportReport Create<T>(Output? output = null) where T : IReportConverter, new()
    {
        return new ExportReport(ReportConverterType.Create<T>(), Output.Get(output));
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is ExportReport other && Equals(other);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (_converter.GetHashCode() * 397) ^ _output.GetHashCode();
        }
    }

    public static bool operator ==(ExportReport? left, ExportReport? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(ExportReport? left, ExportReport? right)
    {
        return !Equals(left, right);
    }
}
