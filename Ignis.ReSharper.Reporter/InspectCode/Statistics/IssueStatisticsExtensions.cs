﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.InspectCode.Statistics;

// ReSharper disable once CheckNamespace
namespace Ignis.ReSharper.Reporter.InspectCode.Reports;

public static class IssueStatisticsExtensions
{
    public static IssueStatistics Statistics(this Report report)
    {
        return new IssueStatistics(report);
    }
}
