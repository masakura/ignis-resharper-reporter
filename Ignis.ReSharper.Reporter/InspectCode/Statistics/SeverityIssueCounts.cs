﻿using System.Collections.Immutable;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Statistics;

public sealed class SeverityIssueCounts : IEquatable<SeverityIssueCounts>
{
    private readonly ImmutableDictionary<Severity, int> _counts;

    public SeverityIssueCounts(IssueCollection issues) :
        this(Severity.All.Select(severity => new KeyValuePair<Severity, int>(severity, issues.Where(severity).Count())))
    {
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public SeverityIssueCounts(IEnumerable<KeyValuePair<Severity, int>> counts) : this(counts.ToImmutableDictionary())
    {
    }

    public SeverityIssueCounts(IDictionary<Severity, int> counts)
    {
        _counts = counts.ToImmutableDictionary();
    }

    public int this[Severity index] => _counts[index];

    public bool Equals(SeverityIssueCounts? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _counts.SequenceEqual(other._counts);
    }

    public override string ToString()
    {
        var query = _counts.Select(pair => $"{pair.Key}: {pair.Value}");
        return string.Join(", ", query);
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is SeverityIssueCounts other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _counts.GetHashCode();
    }

    public static bool operator ==(SeverityIssueCounts? left, SeverityIssueCounts? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(SeverityIssueCounts? left, SeverityIssueCounts? right)
    {
        return !Equals(left, right);
    }
}
