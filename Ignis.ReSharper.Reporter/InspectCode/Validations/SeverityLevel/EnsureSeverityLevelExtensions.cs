﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

public static class EnsureSeverityLevelExtensions
{
    /// <summary>
    ///     Ensure no issues greater than or equals severity.
    /// </summary>
    /// <param name="report"></param>
    /// <param name="severity"></param>
    // ReSharper disable once UnusedMethodReturnValue.Global
    public static Report EnsureNoIssues(this Report report, EnsureSeverityLevel severity)
    {
        severity.EnsureNoIssues(report.Issues.All());
        return report;
    }
}
