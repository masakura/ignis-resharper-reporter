﻿using System.Collections.ObjectModel;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

public sealed class EnsureSeverityLevelCollection
{
    private readonly KeyedCollection _items;

    public EnsureSeverityLevelCollection(IEnumerable<EnsureSeverityLevel> items)
    {
        _items = new KeyedCollection(items);
    }

    public EnsureSeverityLevel Get(Severity severity)
    {
        return Get(severity.ToString());
    }

    public EnsureSeverityLevel Get(string name)
    {
        var key = name.ToLowerInvariant();

        if (_items.Contains(key)) return _items[GetKey(name)];

        throw new ArgumentException($"Must be either {EnsureSeverityLevel.AllItems}. ({name})", nameof(name));
    }

    public override string ToString()
    {
        return string.Join("|", _items.Select(GetKey));
    }

    private static string GetKey(string name)
    {
        return EnsureSeverityLevel.NormalizeName(name);
    }

    private static string GetKey(EnsureSeverityLevel item)
    {
        return GetKey(item.ToString());
    }

    private sealed class KeyedCollection : KeyedCollection<string, EnsureSeverityLevel>
    {
        public KeyedCollection(IEnumerable<EnsureSeverityLevel> items)
        {
            foreach (var item in items) Add(item);
        }

        protected override string GetKeyForItem(EnsureSeverityLevel item)
        {
            return GetKey(item);
        }
    }
}
