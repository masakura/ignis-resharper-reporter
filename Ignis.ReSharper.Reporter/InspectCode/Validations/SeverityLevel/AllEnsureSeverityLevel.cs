﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

internal sealed class AllEnsureSeverityLevel : EnsureSeverityLevel
{
    private AllEnsureSeverityLevel() : base("all")
    {
    }

    public static EnsureSeverityLevel Instance { get; } = new AllEnsureSeverityLevel();

    public override void EnsureNoIssues(IssueCollection issues)
    {
        issues.EnsureNoIssues();
    }
}
