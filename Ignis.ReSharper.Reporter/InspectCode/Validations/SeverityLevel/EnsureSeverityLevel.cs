﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

namespace Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

public abstract class EnsureSeverityLevel
{
    private readonly string _name;

    static EnsureSeverityLevel()
    {
        var severities = Severity.All
            .Select(severity => new SeverityEnsureSeverityLevel(severity, severity.GetAllHigherOrEquals()));

        var items = new[] {All}
            .Union(severities)
            .Union(new[] {None});

        AllItems = new EnsureSeverityLevelCollection(items);
    }

    protected EnsureSeverityLevel(string name)
    {
        _name = NormalizeName(name);
    }

    public static EnsureSeverityLevel All => AllEnsureSeverityLevel.Instance;
    public static EnsureSeverityLevel None => NoneEnsureSeverityLevel.Instance;
    public static EnsureSeverityLevel Default => None;

    public static EnsureSeverityLevelCollection AllItems { get; }

    // ReSharper disable once MemberCanBePrivate.Global
    public static EnsureSeverityLevel From(Severity severity)
    {
        return AllItems.Get(severity);
    }

    public static implicit operator EnsureSeverityLevel(Severity severity)
    {
        return From(severity);
    }

    public override string ToString()
    {
        return _name;
    }

    public static EnsureSeverityLevel Parse(string value)
    {
        return AllItems.Get(value);
    }

    /// <summary>
    ///     Ensure no issues greater than or equals severity.
    /// </summary>
    /// <param name="issues"></param>
    public abstract void EnsureNoIssues(IssueCollection issues);

    internal static string NormalizeName(string name)
    {
        return name.ToLowerInvariant();
    }
}
