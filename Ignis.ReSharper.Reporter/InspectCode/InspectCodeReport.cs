﻿using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Logging;

namespace Ignis.ReSharper.Reporter.InspectCode;

/// <summary>
///     InspectCode report.
/// </summary>
public sealed class InspectCodeReport
{
    private readonly Logger _logger;
    private readonly Report _report;

    private InspectCodeReport(Report report, Logger logger)
    {
        _report = report;
        _logger = logger;
    }

    /// <summary>
    ///     Load InspectCode report.
    /// </summary>
    /// <param name="input">input file path.</param>
    /// <param name="logger">Logger.</param>
    /// <returns>Report.</returns>
    public static InspectCodeReport Load(string input, Logger? logger = null)
    {
        var log = Logger.Get(logger);

        var report = Report.Load(input);
        log.WriteLine($"Load from {input}.");

        return new InspectCodeReport(report, log);
    }

    /// <summary>
    ///     Export report.
    /// </summary>
    /// <param name="export">Export definition.</param>
    /// <returns></returns>
    // ReSharper disable once UnusedMethodReturnValue.Global
    public InspectCodeReport Export(ExportReport export)
    {
        export.Export(_report, _logger);
        return this;
    }

    /// <summary>
    ///     Export report.
    /// </summary>
    /// <param name="exports">Export definition.</param>
    /// <returns></returns>
    // ReSharper disable once UnusedMethodReturnValue.Global
    public InspectCodeReport Export(IEnumerable<ExportReport> exports)
    {
        foreach (var export in exports) Export(export);

        return this;
    }

    /// <summary>
    ///     Throw exception, if code has issue.
    /// </summary>
    /// <param name="severity">Minimal severity.</param>
    /// <returns></returns>
    // ReSharper disable once UnusedMethodReturnValue.Global
    public InspectCodeReport EnsureNoIssues(EnsureSeverityLevel severity)
    {
        _report.EnsureNoIssues(severity);
        return this;
    }
}
