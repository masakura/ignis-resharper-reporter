﻿using System.Collections.Immutable;

namespace Ignis.ReSharper.Reporter.Logging;

public sealed class AggregateLogging : Logger
{
    private readonly IEnumerable<Logger> _items;

    public AggregateLogging(params Logger[] items) : this((IEnumerable<Logger>) items)
    {
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public AggregateLogging(IEnumerable<Logger> items)
    {
        _items = items.ToImmutableArray();
    }

    public override void WriteLine(string message)
    {
        foreach (var logger in _items) logger.WriteLine(message);
    }
}
