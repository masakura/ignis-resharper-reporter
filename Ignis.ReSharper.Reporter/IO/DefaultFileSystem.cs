﻿namespace Ignis.ReSharper.Reporter.IO;

internal sealed class DefaultFileSystem : FileSystem
{
    private DefaultFileSystem()
    {
    }

    public static FileSystem Instance { get; } = new DefaultFileSystem();

    public override void Mkdir(string directory)
    {
        System.IO.Directory.CreateDirectory(directory);
    }

    public override bool ExistsDirectory(string directory)
    {
        return System.IO.Directory.Exists(directory);
    }
}
