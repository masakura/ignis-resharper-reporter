﻿namespace Ignis.ReSharper.Reporter.IO;

internal abstract class FileSystem
{
    public static FileSystem Default => DefaultFileSystem.Instance;
    public abstract void Mkdir(string directory);
    public abstract bool ExistsDirectory(string directory);
}
