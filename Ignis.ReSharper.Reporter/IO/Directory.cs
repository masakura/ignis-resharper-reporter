﻿namespace Ignis.ReSharper.Reporter.IO;

internal sealed class Directory
{
    private readonly FileSystem _fileSystem;
    private readonly string _value;

    public Directory(string value, FileSystem fileSystem)
    {
        _value = value;
        _fileSystem = fileSystem;
    }

    public bool Exists => _fileSystem.ExistsDirectory(_value) || _value is "." or "..";

    public void Mkdirp()
    {
        if (Exists) return;

        Parent().Mkdirp();
        _fileSystem.Mkdir(_value);
    }

    private Directory Parent()
    {
        var directory = GetDirectoryName(_value);
        return new Directory(directory, _fileSystem);
    }

    public static Directory FromFile(string file)
    {
        return FromFile(file, FileSystem.Default);
    }

    internal static Directory FromFile(string file, FileSystem fileSystem)
    {
        var directory = GetDirectoryName(file);
        return new Directory(directory, fileSystem);
    }

    public override string ToString()
    {
        return _value;
    }

    private static string GetDirectoryName(string path)
    {
        var parent = Path.GetDirectoryName(path) ?? throw new InvalidOperationException();
        if (parent == string.Empty) return ".";
        return parent;
    }
}
