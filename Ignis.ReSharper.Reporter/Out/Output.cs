﻿using Ignis.ReSharper.Reporter.Logging;
using Ignis.ReSharper.Reporter.Parameter;

namespace Ignis.ReSharper.Reporter.Out;

public abstract class Output : IDisposable
{
    public abstract void Dispose();
    public abstract Stream Stream();
    public abstract ExportParameters ToParameters();
    public abstract void Complete(Logger logger);

    public static Output File(string path)
    {
        return new FileOutput(path);
    }

    public static Output Logging()
    {
        return LoggingOutput.Instance;
    }

    public static Output Parse(ExportParameters parameters)
    {
        return FileOutput.TryParse(parameters) ?? LoggingOutput.Instance;
    }

    public static Output Get(Output? output)
    {
        return output ?? Logging();
    }
}
