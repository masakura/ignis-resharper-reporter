﻿using System.Linq;

// ReSharper disable once CheckNamespace
namespace System.Collections.Generic;

internal static class EnumerableExtensions
{
    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> items)
    {
        return items.OrderBy(_ => Guid.NewGuid());
    }
}
