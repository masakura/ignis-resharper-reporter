﻿using System;
using System.Collections.Generic;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;

namespace Ignis.ReSharper.Reporter.Validations.EnsureSeverity;

public sealed class EnsureSeverityLevelTest
{
    private readonly ITestOutputHelper _output;

    public EnsureSeverityLevelTest(ITestOutputHelper output)
    {
        _output = output;
    }

    [Theory]
    [MemberData(nameof(ParseSource))]
    public void TestParse(string severity, EnsureSeverityLevel expected)
    {
        var actual = EnsureSeverityLevel.Parse(severity);

        _output.WriteLine(actual.GetHashCode().ToString());
        _output.WriteLine(expected.GetHashCode().ToString());
        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestEnsureNone()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        // do not thrown exception.
        report.EnsureNoIssues(EnsureSeverityLevel.None);
    }

    [Fact]
    public void TestEnsureWarning()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        PAssert.Throws<InvalidOperationException>(() => report.EnsureNoIssues(Severity.Warning));
    }

    [Fact]
    public void TestEnsureError()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        // do not thrown exception.
        report.EnsureNoIssues(Severity.Error);
    }

    [Fact]
    public void TestEnsureAll()
    {
        var report = Report.Load(Fixtures.InspectCode.Solution);

        PAssert.Throws<InvalidOperationException>(() => report.EnsureNoIssues(EnsureSeverityLevel.All));
    }

    public static IEnumerable<object[]> ParseSource()
    {
        yield return new object[] {"all", EnsureSeverityLevel.All};
        yield return new object[] {"ALL", EnsureSeverityLevel.All};
        yield return new object[] {"All", EnsureSeverityLevel.All};
        yield return new object[] {"Error", Severity.Error};
        yield return new object[] {"Warning", Severity.Warning};
        yield return new object[] {"Suggestion", Severity.Suggestion};
        yield return new object[] {"Hint", Severity.Hint};
        yield return new object[] {"Info", Severity.Info};
    }
}
