﻿using System;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class OffsetTest
{
    [Fact]
    public void TestToString()
    {
        var actual = Offset.Parse("10-20");

        PAssert.IsTrue(() => $"{actual}" == "10-20");
    }

    [Fact]
    public void Valid()
    {
        var actual = Offset.Parse("10-20");

        PAssert.IsTrue(() => actual == new Offset(10, 20));
    }

    [Fact]
    public void Null()
    {
        PAssert.Throws<ArgumentNullException>(() => Offset.Parse(null));
    }

    [Fact]
    public void ParseError()
    {
        PAssert.Throws<InvalidOperationException>(() => Offset.Parse("15"));
    }
}
