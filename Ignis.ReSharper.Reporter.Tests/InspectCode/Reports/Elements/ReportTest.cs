﻿using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class ReportTest
{
    [Fact]
    public void Solution()
    {
        var actual = Report.Load(Fixtures.InspectCode.Solution);

        var issueTypes = new[]
        {
            new IssueType("CSharpWarnings::CS8618",
                Severity.Warning,
                "Non-nullable member is uninitialized.",
                new Category("CompilerWarnings", "Compiler Warnings")),
            new IssueType("ClassNeverInstantiated.Global",
                Severity.Suggestion,
                "Class is never instantiated: Non-private accessibility",
                new Category("CodeSmell", "Potential Code Quality Issues")),
            new IssueType("RedundantRecordBody",
                Severity.Warning,
                "Redundant 'record' type declaration body",
                new Category("CodeRedundancy", "Redundancies in Code")),
            new IssueType("UnusedMember.Global",
                Severity.Suggestion,
                "Type member is never used: Non-private accessibility",
                new Category("DeclarationRedundancy", "Redundancies in Symbol Declarations"))
        };
        var factory = new IssueFactory(issueTypes);

        var expected = new Report("213.0.20211208.214651",
            new Information(@".\Ignis.ReSharper.Reporter.sln", new InspectionScope("Solution")),
            issueTypes,
            new[]
            {
                new Project("Ignis.ReSharper.Reporter", new[]
                {
                    factory.Create("ClassNeverInstantiated.Global",
                        "Record 'Information' is never instantiated",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Information.cs", "75-86", 3)),
                    factory.Create("RedundantRecordBody",
                        "Redundant empty 'record' declaration body",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Information.cs", "88-92", 4)),
                    factory.Create("ClassNeverInstantiated.Global",
                        "Record 'IssueType' is never instantiated",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\IssueType.cs", "75-84", 3)),
                    factory.Create("RedundantRecordBody", "Redundant empty 'record' declaration body",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\IssueType.cs", "86-90", 4)),
                    factory.Create("CSharpWarnings::CS8618",
                        "Non-nullable property 'ToolVersion' is uninitialized. Consider declaring the property as nullable.",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Report.cs", "154-165", 7)),
                    factory.Create("CSharpWarnings::CS8618",
                        "Non-nullable property 'Information' is uninitialized. Consider declaring the property as nullable.",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Report.cs", "206-217", 9)),
                    factory.Create("UnusedMember.Global",
                        "Property 'Information' is never used",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Report.cs", "206-217", 9)),
                    factory.Create("CSharpWarnings::CS8618",
                        "Non-nullable property 'IssueTypes' is uninitialized. Consider declaring the property as nullable.",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Report.cs", "256-266", 10)),
                    factory.Create("UnusedMember.Global",
                        "Property 'IssueTypes' is never used",
                        new Location(@"Ignis.ReSharper.Reporter\InspectCode\Xml\Report.cs", "256-266", 10))
                }),
                new Project("_build", new[]
                {
                    factory.Create("UnusedMember.Global",
                        "Property 'Clean' is never used",
                        new Location(@"build\Builds\IClean.cs", "129-134", 8)),
                    factory.Create("UnusedMember.Global",
                        "Property 'Lint' is never used",
                        new Location(@"build\Builds\ILint.cs", "283-287", 11)),
                    factory.Create("UnusedMember.Global",
                        "Property 'Test' is never used",
                        new Location(@"build\Builds\ITest.cs", "256-260", 10))
                })
            });

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void Project()
    {
        var actual = Report.Load(Fixtures.InspectCode.Project);

        var issueTypes = new[]
        {
            new IssueType("UnusedMember.Global", Severity.Suggestion,
                "Type member is never used: Non-private accessibility",
                new Category("DeclarationRedundancy", "Redundancies in Symbol Declarations"))
        };
        var factory = new IssueFactory(issueTypes);

        var expected = new Report("213.0.20211208.214651",
            new Information(new InspectionScope("Solution")),
            issueTypes,
            new[]
            {
                new Project("_build", new[]
                {
                    factory.Create("UnusedMember.Global",
                        "Property 'Clean' is never used",
                        new Location(@"Builds\IClean.cs", "129-134", 8)),
                    factory.Create("UnusedMember.Global",
                        "Property 'Lint' is never used",
                        new Location(@"Builds\ILint.cs", "283-287", 11)),
                    factory.Create("UnusedMember.Global",
                        "Property 'Test' is never used",
                        new Location(@"Builds\ITest.cs", "256-260", 10))
                })
            });

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void NoProblems()
    {
        var actual = Report.Load(Fixtures.InspectCode.NoIssues);

        var expected = new Report("213.0.20211208.214651",
            new Information(new InspectionScope("Solution")));

        PAssert.IsTrue(() => actual == expected);
    }
}
