﻿using System.Xml.Linq;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class IssueTest
{
    private readonly IssueTypeCollection _issueTypes;

    public IssueTest()
    {
        _issueTypes = new[]
        {
            new IssueType("UnusedMember.Global",
                Severity.Suggestion,
                "Type member is never used: Non-private accessibility",
                new Category("DeclarationRedundancy", "Redundancies in Symbol Declarations")),
            new IssueType("RedundantUsingDirective",
                Severity.Warning,
                "Redundant using directive",
                new Category("CodeRedundancy", "Redundancies in Code"))
        };
    }

    [Fact]
    public void TestIssueTypeLevelSeverity()
    {
        var target = Issue(
            @"<Issue TypeId=""UnusedMember.Global"" File=""Builds\IClean.cs"" Offset=""129-134"" Line=""8"" Message=""Property 'Clean' is never used"" />");

        PAssert.IsTrue(() => target.Is(Severity.Suggestion));
    }

    [Fact]
    public void TestIssueLevelSeverity()
    {
        var target = Issue(
            @"<Issue TypeId=""UnusedMember.Global"" File=""Builds\IClean.cs"" Offset=""129-134"" Line=""8"" Message=""Property 'Clean' is never used"" Severity=""WARNING"" />");

        PAssert.IsTrue(() => target.Is(Severity.Warning));
    }

    [Fact]
    public void TestNoLine()
    {
        var target =
            Issue(
                @"<Issue TypeId=""RedundantUsingDirective"" File=""Ignis.ReSharper.Reporter.Nuke\ReSharperReporterTasks.cs"" Offset=""0-54"" Message=""Using directive is not required by the code and can be safely removed"" />");

        PAssert.IsTrue(() => target.Location.Line == 1);
    }

    private Issue Issue(string xml)
    {
        var document = XDocument.Parse(xml);

        return Elements.Issue.Get(document.Root!, _issueTypes);
    }
}
