﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class SeverityTest
{
    [Theory]
    [InlineData("Error", "ERROR")]
    [InlineData("Warning", "WARNING")]
    [InlineData("Suggestion", "SUGGESTION")]
    [InlineData("Hint", "HINT")]
    [InlineData("Info", "INFO")]
    public void SeverityName(string key, string severity)
    {
        var actual = Severity.Get(severity);
        var expected = GetSeverity(key);

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestSort()
    {
        var actual = Severity.All.Shuffle().OrderBy(x => x).ToArray();
        var expected = new[]
        {
            Severity.Info,
            Severity.Hint,
            Severity.Suggestion,
            Severity.Warning,
            Severity.Error
        };

        PAssert.IsTrue(() => actual.SequenceEqual(expected, null));
    }

    [Theory]
    [InlineData(nameof(Severity.Hint))]
    [InlineData(nameof(Severity.Suggestion))]
    public void TestHigherOrEquals(string other)
    {
        var severity = GetSeverity(other);

        PAssert.IsTrue(() => Severity.Suggestion.HigherOrEquals(severity));
    }

    [Theory]
    [InlineData(nameof(Severity.Warning))]
    [InlineData(nameof(Severity.Error))]
    public void TestNotHigherOrEquals(string other)
    {
        var severity = GetSeverity(other);

        PAssert.IsTrue(() => !Severity.Suggestion.HigherOrEquals(severity));
    }

    [Fact]
    public void TestGetHigherOrEquals()
    {
        var actual = Severity.GetHigherOrEquals(Severity.Warning).OrderBy(x => x).ToArray();

        PAssert.IsTrue(() => actual.SequenceEqual(new[] {Severity.Warning, Severity.Error}));
    }

    [Fact]
    public void TestGetHigherOrEqualsAll()
    {
        var actual = Severity.GetHigherOrEquals(null);

        PAssert.IsTrue(() => actual.SequenceEqual(Severity.All));
    }

    private static Severity GetSeverity(string key)
    {
        var property = typeof(Severity)
            .GetProperty(key, BindingFlags.Static | BindingFlags.Public);

        return (Severity) (property?.GetValue(null) ?? throw new InvalidOperationException());
    }
}
