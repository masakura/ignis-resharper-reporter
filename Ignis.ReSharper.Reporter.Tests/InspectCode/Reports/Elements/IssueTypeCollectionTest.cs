﻿using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;

public sealed class IssueTypeCollectionTest
{
    private readonly IssueTypeCollection _target;

    public IssueTypeCollectionTest()
    {
        _target = new IssueTypeCollection(new[]
        {
            new IssueType("CSharpWarnings::CS8618",
                Severity.Warning,
                "Non-nullable member is uninitialized.",
                new Category("CompilerWarnings", "Compiler Warnings")),
            new IssueType("ClassNeverInstantiated.Global",
                Severity.Suggestion,
                "Class is never instantiated: Non-private accessibility",
                new Category("CodeSmell", "Potential Code Quality Issues")),
            new IssueType("RedundantRecordBody",
                Severity.Warning,
                "Redundant 'record' type declaration body",
                new Category("CodeRedundancy", "Redundancies in Code")),
            new IssueType("UnusedMember.Global",
                Severity.Suggestion,
                "Type member is never used: Non-private accessibility",
                new Category("DeclarationRedundancy", "Redundancies in Symbol Declarations"))
        });
    }

    [Fact]
    public void TestFind()
    {
        var actual = _target.Find("ClassNeverInstantiated.Global");

        PAssert.IsTrue(() => actual.Id == "ClassNeverInstantiated.Global");
    }
}
