﻿using System.Collections.Generic;
using Ignis.ReSharper.Reporter.InspectCode.Reports;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Statistics;

public sealed class IssueStatisticsTest
{
    [Fact]
    public void TestStatistics()
    {
        var result = Report.Load(Fixtures.InspectCode.Solution).Statistics();

        var actual = result.Severities;
        var expected = new SeverityIssueCounts(new Dictionary<Severity, int>
        {
            {Severity.Error, 0},
            {Severity.Warning, 5},
            {Severity.Suggestion, 7},
            {Severity.Hint, 0},
            {Severity.Info, 0}
        });

        PAssert.IsTrue(() => actual.Equals(expected));
    }
}
