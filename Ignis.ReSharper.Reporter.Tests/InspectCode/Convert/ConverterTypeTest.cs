﻿using System;
using System.IO;
using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert;

public sealed class ConverterTypeTest
{
    [Fact]
    public void TestCreate()
    {
        var target = ReportConverterType.Create<CodeQualityConverter>();

        // ReSharper disable once OperatorIsCanBeUsed
        PAssert.IsTrue(() => target.Instance().GetType() == typeof(CodeQualityConverter));
    }

    [Fact]
    public void TestTryCreateWithParameter()
    {
        var actual = ReportConverterType.TryCreate(typeof(WithAttribute));

        PAssert.IsTrue(() => actual == new ReportConverterType("exporter1", typeof(WithAttribute)));
    }

    [Fact]
    public void TestTryCreateWithoutParameter()
    {
        var actual = ReportConverterType.TryCreate(typeof(WithoutAttribute));

        PAssert.IsTrue(() => actual == new ReportConverterType(nameof(WithoutAttribute), typeof(WithoutAttribute)));
    }

    [Fact]
    public void TestParse()
    {
        var target = ReportConverterType.Parse("type=codequality");

        var actual = new
        {
            target.Name,
            InstanceType = target.Instance().GetType()
        };

        PAssert.IsTrue(() => actual.Equals(new
        {
            Name = "codequality",
            InstanceType = typeof(CodeQualityConverter)
        }));
    }

    private abstract class MockConverter : IReportConverter
    {
        public void Export(Report report, Stream stream)
        {
            throw new NotSupportedException();
        }
    }

    [ReportConverter("exporter1")]
    private sealed class WithAttribute : MockConverter
    {
    }

    private sealed class WithoutAttribute : MockConverter
    {
    }
}
