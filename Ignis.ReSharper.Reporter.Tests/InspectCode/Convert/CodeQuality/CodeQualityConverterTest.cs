﻿using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

public sealed class CodeQualityConverterTest
{
    private readonly MemoryOutput _output;
    private readonly ReportConverterType _target;

    public CodeQualityConverterTest()
    {
        _target = ReportConverterType.Create<CodeQualityConverter>();
        _output = new MemoryOutput();
    }

    [Fact]
    public void TestExport()
    {
        InspectCodeReport.Load(Fixtures.InspectCode.Project)
            .Export(new ExportReport(_target, _output));
        string actual = new Text(_output.ToString());

        // ReSharper disable once StringLiteralTypo
        string expected = Fixtures.CodeQuality.Project.Text;

        PAssert.IsTrue(() => actual == expected);
    }
}
