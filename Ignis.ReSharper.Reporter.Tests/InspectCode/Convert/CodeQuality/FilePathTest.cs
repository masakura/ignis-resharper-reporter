﻿using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

public sealed class FilePathTest
{
    [Fact]
    public void TestUnixStylePath()
    {
        var target = new FilePath("abc\\def");

        PAssert.IsTrue(() => target.ToString() == "abc/def");
    }
}
