﻿using System.Collections.Generic;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;

public sealed class CodeQualitySeverityTest
{
    [Theory]
    [MemberData(nameof(Map))]
    public void TestToCodeQualitySeverity(Severity resharper, CodeQualitySeverity codeQuality)
    {
        PAssert.IsTrue(() => resharper.ToCodeQualitySeverity() == codeQuality);
    }

    public static IEnumerable<object[]> Map()
    {
        yield return new object[] {Severity.Info, CodeQualitySeverity.Info};
        yield return new object[] {Severity.Hint, CodeQualitySeverity.Info};
        yield return new object[] {Severity.Suggestion, CodeQualitySeverity.Minor};
        yield return new object[] {Severity.Warning, CodeQualitySeverity.Major};
        yield return new object[] {Severity.Error, CodeQualitySeverity.Critical};
    }
}
