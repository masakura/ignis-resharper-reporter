﻿using System;
using System.IO;
using System.Text;
using Ignis.ReSharper.Reporter.Logging;
using Ignis.ReSharper.Reporter.Out;
using Ignis.ReSharper.Reporter.Parameter;

namespace Ignis.ReSharper.Reporter.InspectCode.Convert;

internal sealed class MemoryOutput : Output
{
    private MemoryStream? _stream;
    private string _text = string.Empty;

    public override void Dispose()
    {
        _stream?.Dispose();
    }

    public override Stream Stream()
    {
        _stream?.Dispose();
        _stream = new MemoryStream();
        return _stream;
    }

    public override ExportParameters ToParameters()
    {
        throw new NotSupportedException();
    }

    public override void Complete(Logger logger)
    {
        if (_stream != null) _text = Encoding.UTF8.GetString(_stream.ToArray());
    }

    public override string ToString()
    {
        return _text;
    }
}
