﻿using System;
using System.IO;
using System.Linq;
using Ignis.ReSharper.Reporter.InspectCode.Convert;
using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Reports.Elements;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.InspectCode;

public sealed class ExportReportTest
{
    [Fact]
    public void TestParse()
    {
        var actual = ExportReport.Parse("type=codequality;file=report.json");

        PAssert.IsTrue(() => actual == ExportReport.Create<CodeQualityConverter>("report.json"));
    }

    [Fact]
    public void TestParseMultiple()
    {
        var exports = ExportReport.Parse(new[]
        {
            "type=codequality;file=report.json",
            "type=converter184;file=report184.json"
        });

        var actual = exports.ElementAt(1);

        PAssert.IsTrue(() => actual == ExportReport.Create<Converter184>("report184.json"));
    }


    [ReportConverter("converter184")]
    private sealed class Converter184 : IReportConverter, IEquatable<Converter184>
    {
        public bool Equals(Converter184? other)
        {
            if (other == null) return false;
            return true;
        }

        public void Export(Report report, Stream stream)
        {
            throw new NotSupportedException();
        }

        public override bool Equals(object? obj)
        {
            return ReferenceEquals(this, obj) || obj is Converter184 other && Equals(other);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public static bool operator ==(Converter184? left, Converter184? right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Converter184? left, Converter184? right)
        {
            return !Equals(left, right);
        }
    }
}
