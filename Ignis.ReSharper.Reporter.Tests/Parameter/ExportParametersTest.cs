﻿using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.Parameter;

public sealed class ExportParametersTest
{
    [Fact]
    public void TestParse()
    {
        var target = ExportParameters.Parse("type=codequality;file=codequality.json");

        var actual = new
        {
            Type = target.Value("type"),
            File = target.Value("file")
        };

        PAssert.IsTrue(() => actual.Equals(new
        {
            Type = "codequality",
            File = "codequality.json"
        }));
    }

    [Fact]
    public void TestParseFailed()
    {
        PAssert.Throws<ParameterParseException>(() => ExportParameters.Parse("type"));
    }

    [Fact]
    public void TestAdd()
    {
        var target = new ExportParameters()
            .Add("type", "codequality")
            .Add("file", "codequality.json");

        PAssert.IsTrue(() => target.ToString() == "type=codequality;file=codequality.json");
    }
}
