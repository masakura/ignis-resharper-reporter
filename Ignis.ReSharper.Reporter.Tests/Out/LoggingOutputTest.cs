﻿using System;
using System.IO;
using System.Text;
using Ignis.ReSharper.Reporter.Logging;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;

namespace Ignis.ReSharper.Reporter.Out;

public sealed class LoggingOutputTest : IDisposable
{
    private readonly RecordingLogger _logger;
    private readonly Output _target;

    public LoggingOutputTest()
    {
        _logger = new RecordingLogger();
        _target = Output.Logging();
    }

    public void Dispose()
    {
        _target.Dispose();
    }

    [Fact]
    public void TestStream()
    {
        using var stream = _target.Stream();
        using (var writer = new StreamWriter(stream, leaveOpen: true))
        {
            writer.WriteLine("1");
            writer.WriteLine("2");
        }

        _target.Complete(_logger);

        Text actual = _logger.ToString();
        Text expected = "1\n2\n";

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestParameters()
    {
        PAssert.IsTrue(() => _target.ToParameters().ToString() == string.Empty);
    }

    private sealed class RecordingLogger : Logger
    {
        private readonly StringBuilder _builder = new();

        public override void WriteLine(string message)
        {
            _builder.AppendLine(message);
        }

        public override string ToString()
        {
            return _builder.ToString();
        }
    }
}
