﻿namespace Ignis.ReSharper.Reporter.Testing.CodeQuality;

public sealed class CodeQualityFixtures
{
    public ReportFile Project { get; } = File("project.json");
    public ReportFile SingleIssue { get; } = File("single-issue.json");

    private static ReportFile File(string file)
    {
        return new ReportFile(Path.Combine("CodeQuality", file));
    }
}
