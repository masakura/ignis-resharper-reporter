﻿using System.Text;

namespace Ignis.ReSharper.Reporter.Testing;

/// <summary>
///     `\n` and trim text.
/// </summary>
public sealed class Text : IEquatable<Text>
{
    private readonly string _value;

    public Text(byte[] buffer) : this(GetString(buffer))
    {
    }

    public Text(string value)
    {
        _value = value.Replace("\r\n", "\n").Trim();
    }

    public bool Equals(Text? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _value == other._value;
    }

    public override string ToString()
    {
        return _value;
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is Text other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public static bool operator ==(Text? left, Text? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(Text? left, Text? right)
    {
        return !Equals(left, right);
    }

    public static implicit operator Text(string value)
    {
        return new Text(value);
    }

    public static implicit operator string(Text value)
    {
        return value.ToString();
    }

    private static string GetString(byte[] buffer)
    {
        return Encoding.UTF8.GetString(buffer).Replace("\0", "");
    }
}
