﻿namespace Ignis.ReSharper.Reporter.Testing;

public sealed class ReportFile
{
    private readonly Lazy<Text> _lazy;
    private readonly string _path;

    public ReportFile(string path)
    {
        _path = path;
        _lazy = new Lazy<Text>(() => new Text(File.ReadAllText(path)));
    }

    public Text Text => _lazy.Value;

    public void ForceDelete()
    {
        if (File.Exists(_path)) File.Delete(_path);
    }

    public static implicit operator string(ReportFile file)
    {
        return file._path;
    }

    public override string ToString()
    {
        return _path;
    }
}
