﻿using Ignis.ReSharper.Reporter.Testing.CodeQuality;
using Ignis.ReSharper.Reporter.Testing.InspectCode;

namespace Ignis.ReSharper.Reporter.Testing;

public static class Fixtures
{
    public static InspectCodeFixtures InspectCode { get; } = new();
    public static CodeQualityFixtures CodeQuality { get; } = new();
}
