﻿# Ignis ReSharper Reporter
ReSharper Command Line Tools report converter.

* InspectCode report to GitLab Code Quality JSON.


## Usage
### .NET Tools
Install ReSharper Command Line Tools and Ignis ReSharper Reporter.

```shell
$ dotnet tool install JetBrains.ReSharper.CommandLineTools
$ dotnet tool install Ignis.ReSharper.Reporter.Tool
```

Convert to GitLab Code Quality JSON.

```shell
$ dotnet tool jb inspectcode YourSolution.sln -o=report.xml
$ dotnet tool resharper-reporter --input report.xml \
    --export "type=codequality;file=codequality.json" \
    --export "type=summary"
```

Ensure that there are no issues above the specified warning.

```shell
$ dotnet tool resharper-reporter --input report.xml --severity warning \
    --export "type=codequality;file=codequality.json" \
    --export "type=summary"

...

System.InvalidOperationException: Found problems in code.

...
```

The severity option can be `info`, `hint`, `suggestion`, `warning`, `error` or `all`.


### Support platforms
* .NET 7.0
* .NET 6.0
* .NET 5.0
* .NET Core 3.1
* NUKE 7


### Nuke Build
Install Ignis.ReSharper.Reporter.Nuke.

```
$ dotnet add build package Ignis.ReSharper.Reporter.Nuke
```

Write task.

```csharp
Target Lint => _ => _
    .DependsOn(Compile)
    .Executes(() =>
    {
        ReSharperInspectCode(s => s
            .SetTargetPath(Solution)
            .SetOutput("report.xml"));

        ReSharperReport(s => s
            .SetInput("report.xml")
            // If an Issue is found, the task will fail.
            .SetSeverity(EnsureSeverityLevel.All)
            .AddExport<CodeQualityConverter>("codequality.json")
            .AddExport<SummaryConverter>());
    });
```

#### Support platforms
* .NET Core 3.1 or higher.

## Report converters
### codequality (CodeQualityConverter)
Convert to [GitLab Code Quality JSON report](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool).

Example:

```json
[
  {
    "description": "'unused' is assigned a value but never used.",
    "fingerprint": "7815696ecbf1c96e6894b779456d330e",
    "severity": "minor",
    "location": {
      "path": "lib/index.js",
      "lines": {
        "begin": 42
      }
    }
  }
]
```


### summary (SummaryConverter)
Output summary.

Example:

```
Severity    Count
-----------------
INFO            0
HINT            0
SUGGESTION      7
WARNING         5
ERROR           0
```

## LICENSE
MIT.
