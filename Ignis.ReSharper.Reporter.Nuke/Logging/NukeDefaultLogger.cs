﻿using System.Collections.Generic;
using Ignis.ReSharper.Reporter.Logging;
using Nuke.Common.Tooling;

namespace Ignis.ReSharper.Reporter.Nuke.Logging;

internal sealed class NukeDefaultLogger : Logger
{
    private readonly AggregateLogging _inner;
    private readonly NukeOutputLogger _output;

    public NukeDefaultLogger()
    {
        _output = new NukeOutputLogger();
        _inner = new AggregateLogging(NukeConsoleLogger.Instance, _output);
    }

    public override void WriteLine(string message)
    {
        _inner.WriteLine(message);
    }

    public IReadOnlyCollection<Output> Outputs()
    {
        return _output.Outputs();
    }
}
