﻿using Ignis.ReSharper.Reporter.Logging;
using Serilog;

namespace Ignis.ReSharper.Reporter.Nuke.Logging;

internal sealed class NukeConsoleLogger : Logger
{
    private NukeConsoleLogger()
    {
    }

    public static Logger Instance { get; } = new NukeConsoleLogger();

    public override void WriteLine(string message)
    {
        Log.Information("{Message}", message);
    }
}
