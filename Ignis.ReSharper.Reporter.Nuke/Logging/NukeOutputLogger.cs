﻿using System.Collections.Generic;
using Ignis.ReSharper.Reporter.Logging;
using Nuke.Common.Tooling;

namespace Ignis.ReSharper.Reporter.Nuke.Logging;

internal sealed class NukeOutputLogger : Logger
{
    private readonly List<Output> _outputs = new();

    public override void WriteLine(string message)
    {
        _outputs.Add(new Output {Type = OutputType.Std, Text = message});
    }

    public IReadOnlyCollection<Output> Outputs()
    {
        return _outputs.AsReadOnly();
    }
}
