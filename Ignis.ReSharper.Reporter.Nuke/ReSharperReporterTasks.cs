﻿using System.Collections.Generic;
using Ignis.ReSharper.Reporter.InspectCode;
using Ignis.ReSharper.Reporter.Nuke.Logging;
using Nuke.Common.Tooling;

namespace Ignis.ReSharper.Reporter.Nuke;

public static class ReSharperReporterTasks
{
    /// <summary>
    ///     Process InspectCode report file.
    /// </summary>
    /// <param name="configurator"></param>
    /// <returns></returns>
    public static IReadOnlyCollection<Output> ReSharperReport(Configure<ReSharperReportSettings> configurator)
    {
        return ReSharperReport(configurator(new ReSharperReportSettings()));
    }

    /// <summary>
    ///     Process InspectCode report file.
    /// </summary>
    /// <param name="settings"></param>
    /// <returns></returns>
    // ReSharper disable once MemberCanBePrivate.Global
    public static IReadOnlyCollection<Output> ReSharperReport(ReSharperReportSettings settings)
    {
        var logger = new NukeDefaultLogger();

        var report = InspectCodeReport.Load(settings.Input, logger);

        foreach (var exporter in settings.Exports) report.Export(exporter);

        report.EnsureNoIssues(settings.Severity);

        return logger.Outputs();
    }
}
