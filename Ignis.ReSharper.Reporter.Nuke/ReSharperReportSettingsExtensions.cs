﻿using Ignis.ReSharper.Reporter.InspectCode;
using Ignis.ReSharper.Reporter.InspectCode.Convert;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Out;

namespace Ignis.ReSharper.Reporter.Nuke;

public static class ReSharperReportSettingsExtensions
{
    /// <summary>
    ///     Set InspectCode report file path. (ReSharper Command Line Tools)
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="value">report file path.</param>
    /// <returns></returns>
    public static ReSharperReportSettings SetInput(this ReSharperReportSettings settings, string value)
    {
        settings.Input = value;
        return settings;
    }

    /// <summary>
    ///     Enable throwing exception, if code problems found.
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="severity">minimal severity.</param>
    /// <returns></returns>
    public static ReSharperReportSettings SetSeverity(this ReSharperReportSettings settings,
        EnsureSeverityLevel severity)
    {
        settings.Severity = severity;
        return settings;
    }

    /// <summary>
    ///     Add exporter.
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="outputFile">Output file path.</param>
    /// <typeparam name="T">Report converter type.</typeparam>
    /// <returns></returns>
    public static ReSharperReportSettings AddExport<T>(this ReSharperReportSettings settings, string outputFile)
        where T : IReportConverter, new()
    {
        settings.Exports.Add(ExportReport.Create<T>(outputFile));
        return settings;
    }

    /// <summary>
    ///     Add exporter.
    /// </summary>
    /// <param name="settings"></param>
    /// <param name="output">Output.</param>
    /// <typeparam name="T">Report converter type.</typeparam>
    /// <returns></returns>
    public static ReSharperReportSettings AddExport<T>(this ReSharperReportSettings settings,
        Output? output = null)
        where T : IReportConverter, new()
    {
        settings.Exports.Add(ExportReport.Create<T>(output));
        return settings;
    }
}
