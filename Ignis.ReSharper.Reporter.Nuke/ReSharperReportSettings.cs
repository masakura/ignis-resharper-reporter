﻿using System.Collections.Generic;
using Ignis.ReSharper.Reporter.InspectCode;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;

namespace Ignis.ReSharper.Reporter.Nuke;

public sealed class ReSharperReportSettings
{
    /// <summary>
    ///     Report file path of InspectCode. (ReSharper Command Line Tools)
    /// </summary>
    public string Input { get; set; } = string.Empty;

    public IList<ExportReport> Exports { get; } = new List<ExportReport>();
    public EnsureSeverityLevel Severity { get; set; } = EnsureSeverityLevel.Default;
}
