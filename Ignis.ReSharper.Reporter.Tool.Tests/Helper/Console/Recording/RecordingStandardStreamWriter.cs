﻿using System.CommandLine.IO;
using System.IO;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console.Recording;

internal sealed class RecordingStandardStreamWriter : IStandardStreamWriter
{
    private readonly StreamWriter _writer;

    public RecordingStandardStreamWriter(MemoryStream stream)
    {
        _writer = new StreamWriter(stream, leaveOpen: true);
    }

    public void Write(string value)
    {
        _writer.Write(value);
        _writer.Flush();
    }
}
