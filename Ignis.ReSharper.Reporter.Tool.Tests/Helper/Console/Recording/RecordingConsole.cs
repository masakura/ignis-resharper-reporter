﻿using System.CommandLine;
using System.CommandLine.IO;
using System.IO;
using System.Text;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console.Recording;

internal sealed class RecordingConsole : IConsole
{
    private readonly MemoryStream _stream;

    public RecordingConsole()
    {
        _stream = new MemoryStream();
        Out = new RecordingStandardStreamWriter(_stream);
        Error = new RecordingStandardStreamWriter(_stream);
    }

    public IStandardStreamWriter Out { get; }
    public IStandardStreamWriter Error { get; }
    public bool IsOutputRedirected => false;
    public bool IsErrorRedirected => false;
    public bool IsInputRedirected => false;

    public override string ToString()
    {
        return Encoding.UTF8.GetString(_stream.ToArray());
    }
}
