﻿using System.CommandLine;
using System.CommandLine.IO;
using Xunit.Abstractions;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console.XUniit;

internal sealed class XUnitConsole : IConsole
{
    public XUnitConsole(ITestOutputHelper output)
    {
        Out = new XUnitStreamWriter(output);
        Error = new XUnitStreamWriter(output);
    }

    public IStandardStreamWriter Out { get; }
    public IStandardStreamWriter Error { get; }
    public bool IsOutputRedirected => false;
    public bool IsErrorRedirected => false;
    public bool IsInputRedirected => false;
}
