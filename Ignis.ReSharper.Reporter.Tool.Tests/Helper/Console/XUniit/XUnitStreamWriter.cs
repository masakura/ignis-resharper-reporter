﻿using System.CommandLine.IO;
using System.Text;
using System.Text.RegularExpressions;
using Xunit.Abstractions;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console.XUniit;

internal sealed class XUnitStreamWriter : IStandardStreamWriter
{
    private readonly StringBuilder _buffer = new();
    private readonly ITestOutputHelper _output;

    public XUnitStreamWriter(ITestOutputHelper output)
    {
        _output = output;
    }

    public void Write(string value)
    {
        _buffer.Append(value);

        var text = _buffer.ToString();
        var regex = new Regex("(\r\n|\n)");
        if (!regex.Match(text).Success) return;

        _output.WriteLine(regex.Replace(text, string.Empty));
        _buffer.Clear();
    }
}
