﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.CommandLine;
using System.CommandLine.IO;
using System.Linq;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console;

internal sealed class AggregateStandardStreamWriter : IStandardStreamWriter
{
    private readonly IEnumerable<IStandardStreamWriter> _writers;

    private AggregateStandardStreamWriter(IEnumerable<IStandardStreamWriter> writers)
    {
        _writers = writers.ToImmutableArray();
    }

    public void Write(string value)
    {
        foreach (var writer in _writers) writer.Write(value);
    }

    public static IStandardStreamWriter CreateOut(IEnumerable<IConsole> consoles)
    {
        return new AggregateStandardStreamWriter(consoles.Select(x => x.Out));
    }

    public static IStandardStreamWriter CreateError(ImmutableArray<IConsole> consoles)
    {
        return new AggregateStandardStreamWriter(consoles.Select(x => x.Error));
    }
}
