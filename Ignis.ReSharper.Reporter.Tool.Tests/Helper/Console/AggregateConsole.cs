﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.CommandLine;
using System.CommandLine.IO;

namespace Ignis.ReSharper.Reporter.Tool.Helper.Console;

internal sealed class AggregateConsole : IConsole
{
    public AggregateConsole(params IConsole[] consoles) : this((IEnumerable<IConsole>) consoles)
    {
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public AggregateConsole(IEnumerable<IConsole> consoles)
    {
        var list = consoles.ToImmutableArray();

        Out = AggregateStandardStreamWriter.CreateOut(list);
        Error = AggregateStandardStreamWriter.CreateError(list);
    }

    public IStandardStreamWriter Out { get; }
    public IStandardStreamWriter Error { get; }
    public bool IsOutputRedirected { get; } = false;
    public bool IsErrorRedirected { get; } = false;
    public bool IsInputRedirected { get; } = false;
}
