﻿using System.IO;
using System.Threading.Tasks;
using Ignis.ReSharper.Reporter.Testing;
using Ignis.ReSharper.Reporter.Tool.Helper.Console;
using Ignis.ReSharper.Reporter.Tool.Helper.Console.Recording;
using Ignis.ReSharper.Reporter.Tool.Helper.Console.XUniit;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;

namespace Ignis.ReSharper.Reporter.Tool;

public sealed class ReSharperReporterCommandTest
{
    private readonly ReportFile _actualReport;
    private readonly RecordingConsole _console;
    private readonly ReportFile _input;
    private readonly ReSharperReporterCommand _target;

    public ReSharperReporterCommandTest(ITestOutputHelper output)
    {
        _input = Fixtures.InspectCode.SingleIssue;
        _console = new RecordingConsole();
        _target = new ReSharperReporterCommand(new AggregateConsole(_console, new XUnitConsole(output)));

        _actualReport = new ReportFile("codequality.json");
        _actualReport.ForceDelete();
    }

    [Fact]
    public async Task TestExportToSummary()
    {
        await _target.InvokeAsync(new[]
        {
            "--input",
            _input,
            "--export",
            "type=summary"
        });

        Text actual = _console.ToString();
        Text expected = @$"
Load from {new FileInfo(_input).FullName}.

Severity    Count
-----------------
INFO            0
HINT            0
SUGGESTION      0
WARNING         1
ERROR           0
";

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public async Task TestExportToCodeQuality()
    {
        var exitCode = await _target.InvokeAsync(new[]
        {
            "--input",
            _input,
            "--export",
            "type=codequality;file=codequality.json"
        });

        var actual = new
        {
            ExitCode = exitCode,
            Json = _actualReport.Text
        };
        var expected = new
        {
            ExitCode = 0,
            Json = Fixtures.CodeQuality.SingleIssue.Text
        };

        PAssert.IsTrue(() => actual.Equals(expected));
    }

    [Fact]
    public async Task TestEnsureNoIssues()
    {
        var actual = await _target.InvokeAsync(new[]
        {
            "--input",
            _input,
            "--export",
            "type=codequality;file=codequality.json",
            "--severity",
            "all"
        });

        PAssert.IsTrue(() => actual == 2);
    }
}
