﻿using System;
using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Testing;
using PowerAssert;
using Xunit;
using static Ignis.ReSharper.Reporter.Nuke.ReSharperReporterTasks;

namespace Ignis.ReSharper.Reporter.Nuke;

public sealed class ReSharperReporterTasksTest
{
    [Fact]
    public void TestEnsureNoIssuesFailed()
    {
        // ReSharper disable once ConvertClosureToMethodGroup
        PAssert.Throws<InvalidOperationException>(() => EnsureNoIssues());

        void EnsureNoIssues()
        {
            ReSharperReport(s => s
                .SetInput(Fixtures.InspectCode.SingleIssue)
                .SetSeverity(EnsureSeverityLevel.All));
        }
    }

    [Fact]
    public void TestEnsureNoIssuesSuccess()
    {
        // do not thrown.
        ReSharperReport(s => s
            .SetInput(Fixtures.InspectCode.NoIssues)
            .SetSeverity(EnsureSeverityLevel.All));
    }

    [Fact]
    public void TestCodeQuality()
    {
        ReSharperReport(s => s
            .SetInput(Fixtures.InspectCode.Project)
            .AddExport<CodeQualityConverter>("codequality.json"));

        string actual = new ReportFile("codequality.json").Text;
        string expected = Fixtures.CodeQuality.Project.Text;

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestSummary()
    {
        Text actual = ReSharperReport(s => s
                .SetInput(Fixtures.InspectCode.Solution)
                .AddExport<SummaryConverter>())
            .Read();

        Text expected = $@"
Load from {Fixtures.InspectCode.Solution}.

Severity    Count
-----------------
INFO            0
HINT            0
SUGGESTION      7
WARNING         5
ERROR           0
";

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestOutput()
    {
        Text actual = ReSharperReport(s => s
                .SetInput(Fixtures.InspectCode.Project)
                .AddExport<CodeQualityConverter>("codequality.json"))
            .Read();

        Text expected = @$"
Load from {Fixtures.InspectCode.Project}.

Export to codequality.json.
";

        PAssert.IsTrue(() => actual == expected);
    }
}
