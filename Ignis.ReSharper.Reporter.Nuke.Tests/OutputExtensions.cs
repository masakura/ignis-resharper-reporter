﻿using System.Collections.Generic;
using System.Linq;
using Nuke.Common.Tooling;

namespace Ignis.ReSharper.Reporter.Nuke;

internal static class OutputExtensions
{
    public static string Read(this IEnumerable<Output> outputs)
    {
        var query = outputs
            .Select(output => output.Text);

        return string.Join("\n", query);
    }
}
